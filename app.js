
const app = Vue.createApp({
    data(){
        return{
            principal:"",
            rate:"",
            time:"",
            next:"",
            result1:"",
            result2:"",
            result3:"",
            si:"",
            p1:"",
            r1:"",
            t1:"",
            p2:"",
            r2:"",
            t2:"",
            p3:"",
            r3:"",
            t3:"",
            i:0,
            value:"SI 1"
        }
    },
    methods:{
        // simple_interest1(){
        //     this.si = (Number(this.principal) * Number(this.rate) * Number(this.time)) / 100;
        //     this.result1 = this.si;
        //     this.principal = "";
        //     this.rate = "";
        //     this.time = "";
           
        // },
        // simple_interest2(){
        //     this.si = (Number(this.principal) * Number(this.rate) * Number(this.time)) / 100;
        //     this.result2 = this.si;
        //     this.principal = "";
        //     this.rate = "";
        //     this.time = "";
        // },
        // simple_interest3(){
        //     this.si = (Number(this.principal) * Number(this.rate) * Number(this.time)) / 100;
        //     this.result3 = this.si;
        //     this.principal = "";
        //     this.rate = "";
        //     this.time = "";
        // }
        input(principal, rate, time){
            if (this.i == 0) {
                this.p1 = principal;
                this.r1 = rate;
                this.t1 = time;
                this.si = (Number(this.principal) * Number(this.rate) * Number(this.time)) / 100;
                this.result1 = this.si;
                this.value = "SI 2"
            }
            else if (this.i == 1) {
                this.p2 = principal;
                this.r2 = rate;
                this.t2 = time;
                this.si = (Number(this.principal) * Number(this.rate) * Number(this.time)) / 100;
                this.result2 = this.si;
                this.value = "SI 3"
            }
            else if (this.i == 2) {
                this.p3 = principal;
                this.r3 = rate;
                this.t3 = time;
                this.si = (Number(this.principal) * Number(this.rate) * Number(this.time)) / 100;
                this.result3 = this.si;
                this.value = "Restart";
            }
            else if (this.i == 3) {
                this.result1 = "";
                this.result2 = "";
                this.result3 = "";
                this.principal = "";
                this.rate = "";
                this.time = "";
                this.i = -1;
                this.value = "SI 1"
            }
           
            this.principal = "";
            this.rate = "";
            this.time = "";
            
            this.i ++; 
        }
    }
})
app.mount("#app")