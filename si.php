<?php

    echo "<html>
            <head>
                <link rel='stylesheet' href='bootstrap/bootstap.css'>
                <link rel='stylesheet' href='style.css'>
            </head>
        </html>";

    $p1 = $_GET['p1'];
    $r1 = $_GET['r1'];
    $t1 = $_GET['t1'];

    $p2 = $_GET['p2'];
    $r2 = $_GET['r2'];
    $t2 = $_GET['t2'];

    $p3 = $_GET['p3'];
    $r3 = $_GET['r3'];
    $t3 = $_GET['t3'];

    
    $i = 3; 

    echo "<div class='alert alert-primary' role='alert' id='question'> 
        <h4 class='alert-heading'>Question 34</h4>
        <p>Write a program to calculate SI for 3 sets of p, n, and r.</p>
        <hr>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label for=''>Principal 1: </label>
                        <small class='form-text text-muted'>$p1</small>
                    </div>
                    <div class='form-group'>
                        <label for=''>Rate 1: </label>
                        <small class='form-text text-muted'>$r1</small>
                    </div>
                    <div class='form-group'>
                        <label for=''>Time 1: </label>
                        <small class='form-text text-muted'>$t1</small>
                    </div>
                </div>
                <div class='col-sm-4'>
                    <div class='form-group'>
                        <label for=''>Principal 2: </label>
                        <small class='form-text text-muted'>$p2</small>
                    </div>
                    <div class='form-group'>
                        <label for=''>Rate 2: </label>
                        <small class='form-text text-muted'>$r2</small>
                    </div>
                    <div class='form-group'>
                        <label for=''>Time 2: </label>
                        <small class='form-text text-muted'>$t2</small>
                    </div>
                </div>
                <div class='col-sm-4'>
                    <div>
                        <label for=''>Principal 3: </label>
                        <small class='form-text text-muted'>$p3</small>
                    </div>
                    <div class='form-group'>
                        <label for=''>Rate 3: </label>
                        <small class='form-text text-muted'>$r3</small>
                    </div>
                    <div class='form-group'>
                        <label for=''>Time 3: </label>
                        <small class='form-text text-muted'>$t3</small>
                    </div>
                </div>
            </div>
            <hr>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='card text-center' style='width: 15rem;'>
                        <div class='card-body'>
                            <h5 class='card-title'>Simple Interest</h5>";
                            
                            while ($i <= 3) {
                                $si1 = ($p1 * $r1 * $t1) / 100; 
                                $si2 = ($p2 * $r2 * $t2) / 100;
                                $si3 = ($p3 * $r3 * $t3) / 100;
                                echo "The simple interest = $si1 <br>";
                                echo "The simple interest = $si2 <br>";
                                echo "The simple interest = $si3 <br>";
                                $i++;
                            };

            echo "      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";


?>

